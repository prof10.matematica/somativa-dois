import { BrowserRouter, Route, Routes } from "react-router-dom";
import Principal from './paginas/Principal';
import Cadastro from './paginas/Cadastro';
import Login from './paginas/Login';


const Rotas = () => {
    return(
        
            <BrowserRouter>
                <Routes>
                    <Route exact={true} path="/principal" element={<Principal/>}/>                  
                    <Route exact={true} path="/cadastro" element={<Cadastro/>}/>
                    <Route exact={true} path="/" element={<Login/>}/>                               
                </Routes>               
            
            </BrowserRouter>

            
        
        
    )



}

export default Rotas;