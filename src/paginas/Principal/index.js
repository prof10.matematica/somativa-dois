import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import firebase from '../../Firebase';


class Principal extends Component{
    constructor(props){
      super(props);
      this.state = {
        nome: "",
        sobrenome: "",
        data_de_nascimento: ""

      }
    }

    async componentDidMount(){
      await firebase.auth().onAuthStateChanged(async (usuario) =>{
        if(usuario){
          var uid = usuario.uid;

          await firebase.firestore().collection("usuario").doc(uid).get()
          .then((retorno) =>{
            this.setState({
              nome: retorno.data().nome,
              sobrenome: retorno.data().sobrenome,
              data_de_nascimento: retorno.data().data_de_nascimento
            })
          })
        }

      })
    }

    render(){
      return(
        <div>
            <h1>Página Principal</h1>           
            nome: {this.state.nome} <br/>
            sobrenome: {this.state.sobrenome} <br/>
            data_de_nascimento: {this.state.data_de_nascimento}
        </div>
      )
    }
}

export default Principal;