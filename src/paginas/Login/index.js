import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import firebase from '../../Firebase';

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            senha: ""                       

        }

        this.autenticarUsuario = this.autenticarUsuario.bind(this);
        
    }    

    async autenticarUsuario(){

              await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
              .then(() =>{
                window.location.href = "./principal";
              })
              .catch((erro) => {
                window.alert('Usuário ou senha incorretos!')
              });
       
        
    }   
    

    render(){
        return(
            <div>
                <h1>Página de Login</h1>
                <input type="text" placeholder='Email' onChange={(e) => this.setState({email: e.target.value})}></input>
                <br/>
                <input type="text" placeholder='Senha' onChange={(e) => this.setState({senha: e.target.value})}></input>
                <br/>                
                <button onClick={this.autenticarUsuario}>Entrar</button>                             
                <Link to="/Cadastro"><button>Cadastrar</button></Link>        
               
            </div>
        )
    }
}

export default Login;