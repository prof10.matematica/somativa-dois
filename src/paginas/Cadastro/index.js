import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import firebase from '../../Firebase';

class Cadastro extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            senha: "",
            nome: "",
            sobrenome: "",
            data_de_nascimento: ""           

        }

        this.criarUsuario = this.criarUsuario.bind(this);
        
    }    

    async criarUsuario(){

              
        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then( async (retorno) => {

            await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                nome: this.state.nome,
                sobrenome: this.state.sobrenome,
                data_de_nascimento: this.state.data_de_nascimento

            });
            window.location.href = "./";
        });
        
    }   
    

    render(){
        return(
            <div>
                <h1>Página de Cadastro</h1>
                <input type="text" placeholder='Email' onChange={(e) => this.setState({email: e.target.value})}></input>
                <br/>
                <input type="text" placeholder='Senha' onChange={(e) => this.setState({senha: e.target.value})}></input>
                <br/>
                <input type="text" placeholder='Nome' onChange={(e) => this.setState({nome: e.target.value})}></input>
                <br/>
                <input type="text" placeholder='Sobrenome' onChange={(e) => this.setState({sobrenome: e.target.value})}></input>
                <br/>
                <input type="text" placeholder='Data de Nascimento' onChange={(e) => this.setState({data_de_nascimento: e.target.value})}></input>
                <br/>
                <button onClick={this.criarUsuario}>Cadastrar</button>        
               
            </div>
        )
    }
}

export default Cadastro;